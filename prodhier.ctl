LOAD DATA
INFILE 'G:\Master2\BIG DATA\TP-DW-APB1\prodhier.apb' --"fix 12"
INTO TABLE prodlevel
TRAILING NULLCOLS 
(
Code_level POSITION (1:12) CHAR(12),
Class_level POSITION (13:24) CHAR(12),
Group_level POSITION (25:36) CHAR(12),
Family_level POSITION (37:48) CHAR(12),
Line_level  POSITION (49:60) CHAR(12),
Division_level POSITION (61:72) CHAR(12) 
)
